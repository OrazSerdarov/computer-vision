%read params
clc
clear
close all

aufgabe3 = load('aufgabe_3');
p_est = aufgabe3.p0';

%def function
vz1 = sym('z1',[2 1]);
vz2 = sym('z2',[2 1]);
vx = sym('x',[2 1]);
syms radius
equation=  norm(vx - vz1) + norm(vx -vz2) -radius;


z1_vars = num2cell(vz1);
z2_vars =num2cell(vz2);
J = jacobian(  equation, [z1_vars{:},z2_vars{:},radius]);
res_vc = zeros(30,1);



gaussnewton = true;
mindelta = eps;
stepsize = 0.8;
max_itr = 5;
iterations =0;
p_last = p_est;

% present
figure();
scatter(aufgabe3.xs,aufgabe3.ys, 'LineWidth',2)
hold on


while true
    iterations = iterations + 1;

    %calculate errors
    for i=1:1:30
        res_vc(i) =  subs( equation,{radius, vz1(1),vz1(2),vz2(1),vz2(2) ,vx(1),vx(2) },{p_est(5),p_est(1),p_est(2),p_est(3),p_est(4), aufgabe3.xs(i),aufgabe3.ys(i)});
    end    


    %calculate jacobian matrix
   
    Jakt = zeros(30,5) ;
    for i = 1:1:30
        for j= 1:1:5
        Jakt(i,j) = subs( J(j),{vz1(1),vz1(2),vz2(1),vz2(2),vx(1),vx(2) },{p_est(1),p_est(2),p_est(3),p_est(4), aufgabe3.xs(i),aufgabe3.ys(i)});
        end
    end


   
    if gaussnewton     
        p_est = p_est - Pseudeinverse(Jakt) * res_vc;
    else
        p_est = p_est - stepsize * (Jakt'*res_vc);
    end
    
    err_occured = abs(p_est - p_last) < eps * ones(size(p_est));
    if ( err_occured(1) ||err_occured(2) || err_occured(3) || iterations  > max_itr)
        break;
    end
    
    
    
    Drawellipse(p_est)
end
iterations
p_est
error = res_vc' *res_vc
hold off



