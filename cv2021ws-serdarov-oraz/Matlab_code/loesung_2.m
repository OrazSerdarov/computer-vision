clc
clear
close all

aufgabe2 = load('aufgabe_2.mat');
x1 = aufgabe2.xs;  
x2= aufgabe2.ys;  
ai=2.9; bi=-2.6; ri=6; 
p_est = [ai;bi;ri];
[xSize, ySize] = size(x1);

gaussnewton = true;
mindelta = eps;
stepsize = 0.05;
max_itr = 50;
iterations =0;
p_last = p_est;

% matix A 
A = ones(xSize+1,ySize);
A = A';
A(:,1) = x1(1,:);

Jakt = zeros(ySize ,3);
res_vc=zeros(1,ySize);
syms radius z1 z2 xx1 xx2;
J = jacobian(radius - sqrt( (z1 - xx1)^2  +  (z2 - xx2)^2) , [z1,z2,radius]);


% Beobachtungsvector Y
y = x2';


figure();
scatter(x1,x2,100,'LineWidth',2)
hold on


while true
    iterations = iterations +1;
    drawcircle('Center',[p_est(1),p_est(2)],'Radius',p_est(3),'LineWidth',1,'FaceAlpha',0);
    
    
  
    for i = 1:1:ySize
       res_vc(i)=p_est(3) - sqrt( ((p_est(1) - x1(i))^2 ) + ( (p_est(2) - x2(i))^2 ) );
    end

    Jakt = zeros(ySize ,3);
    % jacobian berechnen
    for i = 1:1:ySize
       for j= 1:1:3
           Jakt(i,j) = subs(J(j),{z1,z2,xx1,xx2},{p_est(1),p_est(2),x1(i),x2(i)});

       end
    end
    
 

    % p neu berechnen
    if gaussnewton     
        p_est = p_est - Pseudeinverse(Jakt)*res_vc';
    else
        p_est = p_est - stepsize * (Jakt'*res_vc');
    end
    
    err_occured = abs(p_est - p_last) < eps * ones(size(p_est));
    if ( err_occured(1) ||err_occured(2) || err_occured(3) || iterations  > max_itr)
        break;
    end
   
    p_last= p_est;
end
p_est
error = res_vc * res_vc'
hold off


