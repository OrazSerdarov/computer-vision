clc
clear
close all
aufgabe1 = load('aufgabe_1.mat');
x_array = aufgabe1.xs;
y_array = aufgabe1.ys;

XSize = size(x_array);

% matix A 
A = ones(XSize(1)+1,XSize(2));
A = A';
A(:,1) = x_array(1,:);


% Beobachtungsvector Y
y = y_array';


% Singulärwertzerlegung
[U, S,V] = svd(A);

% PseudeInverse 
Ssize = size(S);
ind = 1:Ssize(1)+1:(Ssize(1)*Ssize(2));
S(ind)=diag(S).^-1;
AP= V *S' *U';


p = AP *y;

% Fehler berechnen
r = y - A*p;
error = r' * r
p

scatter(x_array,y_array)
hold on
result_function = @(input) p(1)*input + p(2);
fplot(result_function,[-30 25])



