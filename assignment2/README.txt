Instructions: 

1. Go to your Google drive.
2. Create the folders "computer-vision/assignment2"
3. Upload the content of this archive (pynn_student.ipynb and res/get_datasets.sh) to the folder "computer-vision/assignment2".
4. Open pynn_student.ipynb in Google Colab and follow the instructions in the notebook. 